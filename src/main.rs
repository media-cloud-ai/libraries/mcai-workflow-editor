mod download_workflow;
mod footer;
mod header;
mod manage_worker_definitions;

use crate::{header::HeaderMessage, manage_worker_definitions::ManageWorkerDefinitionsMessage};
use by_address::ByAddress;
use footer::Footer;
use header::Header;
use manage_worker_definitions::ManageWorkerDefinitions;
use mcai_models::{Icon, WorkerDefinition, Workflow, WorkflowDefinition};
use mcai_workflow::*;
use rexie::*;
use semver::Version;
use serde::Serialize;
use serde_wasm_bindgen::Serializer;
use std::sync::{Arc, Mutex};
use wasm_bindgen::JsValue;
use yew::prelude::*;

static STORE_NAME: &str = "worker-definitions";

pub mod built_info {
  include!(concat!(env!("OUT_DIR"), "/built.rs"));
}

fn main() {
  wasm_logger::init(wasm_logger::Config::default());
  yew::start_app::<Editor>();
}

pub enum EditorMessage {
  Header(HeaderMessage),
  WorkerDefinitions(Vec<(bool, SharedWorkerDefinition)>),
  WorkerDefinition(WorkerDefinitionEvent),
  WorkflowDefinition(WorkflowGraphEvent),
  ManageWorkerDefinitions(ManageWorkerDefinitionsMessage),
  ErrorModal(ModalMessage),
}

pub struct Editor {
  display_configuration: bool,
  current_workflow: SharedWorkflow,
  selected_worker_definition: Option<SharedWorkerDefinition>,
  selected_definition_step_id: Option<u32>,
  worker_definitions: Vec<(bool, SharedWorkerDefinition)>,
  error_modal: Option<Html>,
}

impl Editor {
  async fn database() -> Rexie {
    let database_version = 1;

    Rexie::builder("mcai")
      .version(database_version)
      .add_object_store(ObjectStore::new(STORE_NAME).auto_increment(false))
      .build()
      .await
      .unwrap()
  }

  async fn list_worker_definitions() -> Vec<(bool, SharedWorkerDefinition)> {
    let rexie = Self::database().await;

    let transaction = rexie
      .transaction(&[STORE_NAME], TransactionMode::ReadOnly)
      .unwrap();

    let worker_definitions = transaction.store(STORE_NAME).unwrap();

    let worker_definitions = worker_definitions
      .get_all(None, Some(5), None, None)
      .await
      .unwrap()
      .into_iter()
      .map(|pair| pair.1);

    [
      include_str!("worker_definition/adm_engine_worker.json"),
      include_str!("worker_definition/command_line_worker.json"),
      include_str!("worker_definition/media_probe_worker.json"),
      include_str!("worker_definition/rs_transfer_worker.json"),
    ]
    .iter()
    .map(|worker_definition| serde_json::from_str(worker_definition).unwrap())
    .map(|worker_definition| (false, ByAddress(Arc::new(Mutex::new(worker_definition)))))
    .chain(worker_definitions.filter_map(|value| {
      serde_wasm_bindgen::from_value::<WorkerDefinition>(value)
        .ok()
        .map(|w| (true, ByAddress(Arc::new(Mutex::new(w)))))
    }))
    .collect()
  }

  async fn add_worker_definitions_in_storage(worker_definition: &WorkerDefinition) {
    let rexie = Self::database().await;

    let transaction = rexie
      .transaction(&[STORE_NAME], TransactionMode::ReadWrite)
      .unwrap();

    let worker_definitions = transaction.store(STORE_NAME).unwrap();

    let serialized_worker_definition = worker_definition
      .serialize(&Serializer::json_compatible())
      .unwrap();

    let _worker_definition_id = worker_definitions
      .add(
        &serialized_worker_definition,
        Some(&JsValue::from_str(&format!(
          "{}_{}",
          worker_definition.description.label, worker_definition.description.version
        ))),
      )
      .await
      .unwrap();

    transaction.done().await.unwrap();
  }

  async fn delete_worker_definitions_in_storage(label: &str, version: &Version) {
    let rexie = Self::database().await;

    let transaction = rexie
      .transaction(&[STORE_NAME], TransactionMode::ReadWrite)
      .unwrap();

    let worker_definitions = transaction.store(STORE_NAME).unwrap();

    let r = worker_definitions
      .get_all(
        Some(&KeyRange::only(&JsValue::from_str(&format!("{}_{}", label, version))).unwrap()),
        None,
        None,
        None,
      )
      .await
      .unwrap();

    if let Some(r) = r.first() {
      worker_definitions.delete(&r.0).await.unwrap();
    }

    transaction.done().await.unwrap();
  }

  fn display_error_modal(&mut self, ctx: &Context<Self>, title: &str, message: &str) {
    self.error_modal = Some(html!(
      <Modal
        event={ctx.link().callback(EditorMessage::ErrorModal)}
        height="50vh" width="19vw"
        modal_title={title.to_string()}>
        <div class="error">
          {message}
        </div>
      </Modal>
    ));
  }
}

impl Component for Editor {
  type Message = EditorMessage;
  type Properties = ();

  fn create(ctx: &Context<Self>) -> Self {
    let mut current_workflow = WorkflowDefinition::new("my_workflow", "My Workflow");
    match &mut current_workflow {
      WorkflowDefinition::Version1_8(workflow) => {
        workflow.common.icon = Icon {
          icon: Some("settings".to_string()),
        };
      }
      WorkflowDefinition::Version1_9(workflow) => {
        workflow.common.icon = Icon {
          icon: Some("settings".to_string()),
        };
      }
      WorkflowDefinition::Version1_10(workflow) => {
        workflow.common.icon = Icon {
          icon: Some("settings".to_string()),
        };
      }
      WorkflowDefinition::Version1_11(workflow) => {
        workflow.common.icon = Icon {
          icon: Some("settings".to_string()),
        };
      }
    }

    let current_workflow = ByAddress(Arc::new(Mutex::new(Workflow::Definition(current_workflow))));

    ctx.link().send_future(async {
      EditorMessage::WorkerDefinitions(Self::list_worker_definitions().await)
    });

    let worker_definitions = [
      include_str!("worker_definition/adm_engine_worker.json"),
      include_str!("worker_definition/command_line_worker.json"),
      include_str!("worker_definition/media_probe_worker.json"),
      include_str!("worker_definition/rs_transfer_worker.json"),
    ]
    .iter()
    .map(|worker_definition| serde_json::from_str(worker_definition).unwrap())
    .map(|worker_definition| (false, ByAddress(Arc::new(Mutex::new(worker_definition)))))
    .collect();

    Self {
      display_configuration: false,
      current_workflow,
      selected_worker_definition: None,
      selected_definition_step_id: None,
      worker_definitions,
      error_modal: None,
    }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      EditorMessage::Header(HeaderMessage::DownloadWorkflow) => {
        download_workflow::download_workflow(self.current_workflow.clone());
        false
      }
      EditorMessage::Header(HeaderMessage::OpenWorkflow(workflow_definition)) => {
        self.current_workflow = ByAddress(Arc::new(Mutex::new(Workflow::Definition(
          *workflow_definition,
        ))));
        self.selected_definition_step_id = None;
        self.selected_worker_definition = None;
        true
      }
      EditorMessage::Header(HeaderMessage::WorkflowError(error_message)) => {
        self.display_error_modal(ctx, "Workflow error", &error_message);
        true
      }
      EditorMessage::Header(HeaderMessage::ToggleConfiguration) => {
        self.display_configuration = !self.display_configuration;
        true
      }
      EditorMessage::WorkerDefinitions(worker_definitions) => {
        self.worker_definitions = worker_definitions;
        true
      }
      EditorMessage::ManageWorkerDefinitions(
        ManageWorkerDefinitionsMessage::NewWorkerDefinition(worker_definition),
      ) => {
        ctx.link().send_future(async move {
          Self::add_worker_definitions_in_storage(&worker_definition).await;
          EditorMessage::WorkerDefinitions(Self::list_worker_definitions().await)
        });
        false
      }
      EditorMessage::ManageWorkerDefinitions(
        ManageWorkerDefinitionsMessage::DeleteWorkerDefinition(label, version),
      ) => {
        ctx.link().send_future(async move {
          Self::delete_worker_definitions_in_storage(&label, &version).await;
          EditorMessage::WorkerDefinitions(Self::list_worker_definitions().await)
        });
        false
      }
      EditorMessage::ManageWorkerDefinitions(
        ManageWorkerDefinitionsMessage::WorkerDefinitionError(error_message),
      ) => {
        self.display_error_modal(ctx, "Worker definition error", &error_message);
        true
      }
      EditorMessage::WorkerDefinition(WorkerDefinitionEvent::WorkerSelected(worker_definition)) => {
        self.selected_worker_definition = Some(worker_definition);
        self.selected_definition_step_id = None;
        true
      }
      EditorMessage::WorkerDefinition(WorkerDefinitionEvent::WorkerDeselected) => {
        self.selected_worker_definition = None;
        true
      }
      EditorMessage::WorkflowDefinition(WorkflowGraphEvent::StepSelected(step_id)) => {
        self.selected_definition_step_id = Some(step_id);
        self.selected_worker_definition = None;
        true
      }
      EditorMessage::WorkflowDefinition(WorkflowGraphEvent::StepDeselected) => {
        self.selected_definition_step_id = None;
        true
      }
      EditorMessage::ErrorModal(_) => {
        self.error_modal = None;
        true
      }
    }
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let height = "calc(100vh - 100px)";

    let panel = self
      .selected_worker_definition
      .as_ref()
      .map(|worker_definition| html!(
        <WorkerDetailsPanel height={height} width="20%" worker_definition={worker_definition.clone()} />
      ))
      .unwrap_or_else(|| html!(
        <WorkflowPanel workflow={self.current_workflow.clone()} height={height} width="20%" step_id={self.selected_definition_step_id} />
      ));

    let error_modal = self.error_modal.as_ref().cloned().unwrap_or_default();

    let inner = if self.display_configuration {
      html!(
        <ManageWorkerDefinitions height={height} width="100%" worker_definitions={self.worker_definitions.clone()} events={ctx.link().callback(EditorMessage::ManageWorkerDefinitions)}/>
      )
    } else {
      html!(
        <>
        <WorkerDefinitionPanel height={height} width="20%"
          worker_definitions={self.worker_definitions.iter().map(|w| w.1.clone()).collect::<Vec<_>>()}
          events={ctx.link().callback(EditorMessage::WorkerDefinition)}
          selected_worker_definition={self.selected_worker_definition.clone()}
          />
        <WorkflowGraph height={height} width="60%"
          workflow={self.current_workflow.clone()}
          selected_step={self.selected_definition_step_id}
          events={ctx.link().callback(EditorMessage::WorkflowDefinition)}
          />
        {panel}
        {error_modal}
        </>
      )
    };

    html!(
      <>
        <Header events={ctx.link().callback(EditorMessage::Header)} />
        {inner}
        <Footer />
      </>
    )
  }
}
