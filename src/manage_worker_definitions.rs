use css_in_rust_next::Style;
use mcai_models::WorkerDefinition;
use mcai_workflow::{Button, SharedWorkerDefinition};
use semver::Version;
use wasm_bindgen::JsCast;
use web_sys::{Event, HtmlInputElement};
use yew::{html, Callback, Component, Context, Html, Properties};
use yew_feather::{trash_2::Trash2, upload::Upload};

pub enum InternalMessage {
  SelectedWorkerDefinitionInput(Event),
  OpenWorkerDefinition(Box<serde_json::Result<WorkerDefinition>>),
  Delete(String, Version),
}

pub enum ManageWorkerDefinitionsMessage {
  NewWorkerDefinition(Box<WorkerDefinition>),
  DeleteWorkerDefinition(String, Version),
  WorkerDefinitionError(String),
}

#[derive(PartialEq, Properties)]
pub struct ManageWorkerDefinitionsProperties {
  pub height: String,
  pub width: String,
  pub worker_definitions: Vec<(bool, SharedWorkerDefinition)>,
  pub events: Callback<ManageWorkerDefinitionsMessage>,
}

pub struct ManageWorkerDefinitions {
  style: Style,
}

impl Component for ManageWorkerDefinitions {
  type Message = InternalMessage;
  type Properties = ManageWorkerDefinitionsProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("manage_worker_definitions.css")).unwrap();

    Self { style }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      InternalMessage::SelectedWorkerDefinitionInput(event) => {
        event.prevent_default();
        if let Some(input_element) = event
          .target()
          .and_then(|t| t.dyn_into::<HtmlInputElement>().ok())
        {
          if let Some(file) = input_element.files().and_then(|f| f.get(0)) {
            ctx.link().send_future(async move {
              let blob = mio_gloo_file::Blob::from(file);
              let data = mio_gloo_file::futures::read_as_text(&blob).await.unwrap();
              let worker_definition = serde_json::from_str(&data);
              InternalMessage::OpenWorkerDefinition(Box::new(worker_definition))
            });
          }
        }
      }
      InternalMessage::OpenWorkerDefinition(event) => match *event {
        Ok(worker_definition) => {
          ctx
            .props()
            .events
            .emit(ManageWorkerDefinitionsMessage::NewWorkerDefinition(
              Box::new(worker_definition),
            ));
        }
        Err(error) => {
          log::error!("Unable to load workflow: {:?}", error);
          ctx
            .props()
            .events
            .emit(ManageWorkerDefinitionsMessage::WorkerDefinitionError(
              error.to_string(),
            ));
        }
      },
      InternalMessage::Delete(label, version) => {
        ctx
          .props()
          .events
          .emit(ManageWorkerDefinitionsMessage::DeleteWorkerDefinition(
            label, version,
          ));
      }
    }

    false
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    let style = format!(
      "height: {}; width: {};",
      ctx.props().height,
      ctx.props().width,
    );

    let worker_definitions: Html = ctx
      .props()
      .worker_definitions
      .iter()
      .map(|(in_store, worker_definition)| {
        let worker_definition = worker_definition.lock().unwrap();

        let label = worker_definition.description.label.clone();
        let version = worker_definition.description.version.clone();
        let action = in_store.then(|| html!(
          <Button
            label=""
            icon={html!(<Trash2 />)}
            onclick={ctx.link().callback(move |_| InternalMessage::Delete(label.clone(), version.clone()))}
            />
        )).unwrap_or_default();

        html!(
          <div class="workerDefinition">
            <span class="label">{&worker_definition.description.label}</span>
            <span class="description">{&worker_definition.description.short_description}</span>
            <span>{"v"}{&worker_definition.description.version}</span>
            <span>{action}</span>
          </div>
        )
      })
      .collect();

    html!(
      <div class={self.style.clone()} style={style}>
        <div class="workerDefinitions">
          <h2>{"Workers"}</h2>
          <div class="workerDefinition header">
            <span class="label">{"Label"}</span>
            <span class="description">{"Description"}</span>
            <span>{"Version"}</span>
            <span></span>
          </div>
          {worker_definitions}
        </div>
        <div class="importButton">
          <input type="file" onchange={ctx.link().callback(InternalMessage::SelectedWorkerDefinitionInput)} />
          <Upload />
          {"Import Worker definition"}
        </div>
      </div>
    )
  }
}
