use css_in_rust_next::Style;
use mcai_models::WorkflowDefinition;
use mcai_workflow::McaiLogo;
use wasm_bindgen::JsCast;
use web_sys::{Event, HtmlInputElement};
use yew::prelude::*;
use yew_feather::{download::Download, settings::Settings, upload::Upload};

#[derive(PartialEq, Properties)]
pub struct HeaderProperties {
  pub events: Callback<HeaderMessage>,
}

pub enum HeaderMessage {
  OpenWorkflow(Box<WorkflowDefinition>),
  DownloadWorkflow,
  ToggleConfiguration,
  WorkflowError(String),
}

pub enum HeaderInternalMessage {
  SelectedWorkflowInput(Event),
  OpenWorkflow(Box<serde_json::Result<WorkflowDefinition>>),
  DownloadWorkflow(MouseEvent),
  ToggleConfiguration,
}

pub struct Header {
  style: Style,
}

impl Component for Header {
  type Message = HeaderInternalMessage;
  type Properties = HeaderProperties;

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("header.css")).unwrap();

    Self { style }
  }

  fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
    match msg {
      HeaderInternalMessage::SelectedWorkflowInput(event) => {
        event.prevent_default();
        if let Some(input_element) = event
          .target()
          .and_then(|t| t.dyn_into::<HtmlInputElement>().ok())
        {
          if let Some(file) = input_element.files().and_then(|f| f.get(0)) {
            ctx.link().send_future(async move {
              let blob = mio_gloo_file::Blob::from(file);
              let data = mio_gloo_file::futures::read_as_text(&blob).await.unwrap();
              let workflow_definition = serde_json::from_str(&data);
              HeaderInternalMessage::OpenWorkflow(Box::new(workflow_definition))
            });
          }
        }
      }
      HeaderInternalMessage::OpenWorkflow(event) => match *event {
        Ok(workflow_definition) => {
          ctx
            .props()
            .events
            .emit(HeaderMessage::OpenWorkflow(Box::new(workflow_definition)));
        }
        Err(error) => {
          log::error!("Unable to load workflow: {:?}", error);
          ctx
            .props()
            .events
            .emit(HeaderMessage::WorkflowError(error.to_string()));
        }
      },
      HeaderInternalMessage::DownloadWorkflow(event) => {
        event.prevent_default();
        ctx.props().events.emit(HeaderMessage::DownloadWorkflow);
      }
      HeaderInternalMessage::ToggleConfiguration => {
        ctx.props().events.emit(HeaderMessage::ToggleConfiguration);
      }
    }

    false
  }

  fn view(&self, ctx: &Context<Self>) -> Html {
    html!(
      <div class={self.style.clone()}>
        <McaiLogo height="35px" />
        <span class="title">
          {"MCAI Workflow Editor"}
        </span>
        <span class="menu source">
          <input type="file" id="fileInput" onchange={ctx.link().callback(HeaderInternalMessage::SelectedWorkflowInput)}/>
          <Upload />
          {"Open Workflow"}
        </span>
        <span class="menu" onclick={ctx.link().callback(HeaderInternalMessage::DownloadWorkflow)}>
          <Download />
          {"Save Workflow"}
        </span>
        <span class="filler">
        </span>
        <span class="menu" onclick={ctx.link().callback(|_| HeaderInternalMessage::ToggleConfiguration)}>
          <Settings />
          {"Configure"}
        </span>
      </div>
    )
  }
}
