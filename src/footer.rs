use css_in_rust_next::Style;
use yew::prelude::*;

pub struct Footer {
  style: Style,
}

impl Component for Footer {
  type Message = ();
  type Properties = ();

  fn create(_ctx: &Context<Self>) -> Self {
    let style = Style::create("Component", include_str!("footer.css")).unwrap();

    Self { style }
  }

  fn view(&self, _ctx: &Context<Self>) -> Html {
    let version = crate::built_info::PKG_VERSION;

    html!(
      <div class={self.style.clone()}>
        <span class="version">
          {"v"}{version}
        </span>
      </div>
    )
  }
}
