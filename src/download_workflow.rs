use mcai_models::Workflow;
use mcai_workflow::SharedWorkflow;
use std::ops::Deref;
use wasm_bindgen::{JsCast, JsValue};
use web_sys::{Blob, BlobPropertyBag, HtmlElement, Url};

pub fn download_workflow(workflow: SharedWorkflow) {
  if let Workflow::Definition(definition) = workflow.lock().unwrap().deref() {
    let js_value = js_sys::Array::from(&JsValue::from_str(
      &serde_json::to_string(&definition).unwrap(),
    ));

    let blob = Blob::new_with_blob_sequence_and_options(
      &js_value,
      BlobPropertyBag::new().type_("application/json"),
    )
    .unwrap();
    let url = Url::create_object_url_with_blob(&blob).unwrap();

    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();
    let a = document.create_element("a").unwrap();
    a.set_attribute("href", &url).unwrap();
    a.set_attribute("style", "display: none").unwrap();

    let version = definition.version().to_string();

    a.set_attribute(
      "download",
      &format!("workflow_{}_v{}.json", definition.identifier(), version),
    )
    .unwrap();

    document.body().unwrap().append_child(&a).unwrap();
    a.dyn_into::<HtmlElement>().unwrap().click();
    Url::revoke_object_url(&url).unwrap();
  }
}
